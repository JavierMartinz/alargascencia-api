<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$app->get('contact', function () use ($app) {
    $response = Cache::get('contact', function () {
        $about_us = [
            'info'  => 'Amigos de la Tierra somos una asociación ecologista que tiene la misión de fomentar el cambio local y global hacia una sociedad respetuosa con el medio ambiente, justa y solidaria.',
            'tlf'   => ['913069900', '913069921'],
            'email' => 'tierra@tierra.org',
            'web'   => 'tierra.org'
        ];

        $places = [
            [
                'name'  => 'AMIGOS DE LA TIERRA ANDALUCÍA',
                'email' => 'andalucia@tierra.org',
                'web'   => 'andalucia.tierra.org',
            ],
            [
                'name'  => 'AMIGOS DE LA TIERRA ARAGÓN',
                'tlf'   => '976274988',
                'email' => 'aragon@tierra.org',
                'web'   => 'aragon.tierra.org',
            ],
            [
                'name'  => 'AMICS DE LA TERRA MALLORCA',
                'tlf'   => '971757939',
                'email' => 'mallorca@tierra.org',
                'web'   => 'mallorca.tierra.org',
            ],
            [
                'name'  => 'AMIGOS DE LA TIERRA LA RIOJA',
                'email' => 'larioja@tierra.org',
                'web'   => 'larioja.tierra.org',
            ],
            [
                'name'  => 'AMIGOS DA TERRA GALICIA',
                'tlf'   => '988374318',
                'email' => 'galicia@tierra.org',
                'web'   => 'galicia.tierra.org',
            ],
            [
                'name'  => 'AMICS DE LA TERRA EIVISSA',
                'tlf'   => '971317486',
                'email' => 'terraeivissa@tierra.org',
                'web'   => 'eivissa.tierra.org',
            ],
            [
                'name'  => 'AMIGOS DE LA TIERRA MADRID',
                'email' => 'madrid@tierra.org',
                'web'   => 'madrid.tierra.org'
            ],
        ];

        return compact('about_us', 'places');
    });

    return $response;
});


$app->get('what-is-this', function () use ($app) {
    $response = Cache::get('what-is-this', function () {
        $sql = 'SELECT language, body_value
            FROM field_data_body
            WHERE entity_id = 17 AND deleted = 0';

        return DB::select($sql);
    });

    return $response;
});

$app->get('places', function () use ($app) {
    $response = Cache::get('places', function () {
        $sql = "SELECT
                    n.nid,
                    n.title,
                    d.field_direccion_thoroughfare as address,
                    d.field_direccion_premise as address_extra,
                    p.field_phone_value as phone,
                    ce.field_correo_electronico_email as email,
                    d.field_direccion_postal_code as postal_code,
                    d.field_direccion_locality as city,
                    d.field_direccion_administrative_area as province,
                    ot.field_open_time_value as open_time,
                    w.field_website_value as website,
                    c.field_coordinates_lat as latitude,
                    c.field_coordinates_lon as longitude,
                    b.body_value as description,
                    GROUP_CONCAT(DISTINCT(nf.name_field_value)) as categories,
                    REPlACE(f.uri, 'public://', 'https://alargascencia.org/sites/default/files/styles/image-marker/public/') as image
                FROM node n
                LEFT JOIN field_data_field_coordinates c ON c.entity_id = n.nid
                LEFT JOIN field_data_field_direccion d ON d.entity_id = n.nid
                LEFT JOIN field_data_field_phone p ON p.entity_id = n.nid
                LEFT JOIN field_data_field_correo_electronico ce ON ce.entity_id = n.nid
                LEFT JOIN field_data_field_open_time ot ON ot.entity_id = n.nid
                LEFT JOIN field_data_field_website w ON w.entity_id = n.nid
                LEFT JOIN field_data_body b ON b.entity_id = n.nid
                LEFT JOIN field_data_field_image fi ON fi.entity_id = n.nid
                LEFT JOIN file_managed f ON f.fid = fi.field_image_fid
                LEFT JOIN field_data_field_category cat ON cat.entity_id = n.nid
                LEFT JOIN field_data_name_field nf ON nf.entity_id = cat.field_category_tid
                WHERE n.type = 'business' AND nf.language = 'es'
                GROUP BY n.nid";

        return DB::select($sql);
    });


    return $response;
});
